*Instalar Docker
#Certifique-se de que não existe nenhuma versão antiga instalada removendo-a
sudo apt-get remove docker docker-engine docker.io containerd runc

echo "Remoção ocorreu com sucesso"

#Atualize o repositório
sudo apt-get update

echo "Atualização feita com sucesso"

#Instale pacotes necessários
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y

echo "Instalação dos Pacotes feita com sucesso"

#Adicione a chave oficial do Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo "Chave oficial adicionada com sucesso"

#Verifique se a chave foi adicionada
sudo apt-key fingerprint 0EBFCD88

echo "verificação de chave efetuada"

#Adicione o repositório do Docker
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

echo "repositorio docker adicionado com sucesso"

#Instale o Docker
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

echo "instalação do docker efetuada!"

#Verifique se o Docker foi corretamente instalado executando no terminal
docker version

echo "verificação de versão do docker efetuada"

#Vamos adicionar o nosso usuário ao grupo do docker
sudo usermod -aG docker $USER

newgrp docker

echo "usuario adiconado ao grupo docker"


*Instalar o Kubernetes

#Preparação do Ambiente do Kubernets para linux

#Baixar o instalador que está disponível na área de downloads

#Acessar o diretorio de downloads, abrir o terminal e executar:

curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"

chmod +x kubectl

sudo mv kubectl /usr/local/bin/

#Verificar se o aplictivo foi instalado corretamente:

kubectl version --client

#Executar o instalador do Minikube

#OBS: Se necessário, reiniciar o computador

sudo dpkg -i minikube_latest_amd64.deb

#Configurar o docker como driver padrão (container runtime) para o Kubernetes 

minikube config set driver docker

#Inicializar um cluster

minikube start